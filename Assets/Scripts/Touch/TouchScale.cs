using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

[RequireComponent(typeof(TouchSelectable))]
public class TouchScale : MonoBehaviour
{
    public float minDistanceDelta;
    public float scaleSensitivity = 6;
    public float minScale = 0.1f;
    public float maxScale = 5;
    private float distanceDelta;
    private TouchSelectable selectable;
    private Vector3 scaleChangeVector;
    void Awake()
    {
        selectable = GetComponent<TouchSelectable>();
        scaleChangeVector = new Vector3();
    }


    void Update()
    {
        if (selectable.isSelected && Input.touchCount == 2)
        {
            if ((Input.touches[0].phase != TouchPhase.Moved || Input.touches[1].phase != TouchPhase.Moved) ||
                Mathf.Abs(distanceDelta = CalculateDistanceDelta(Input.touches[0], Input.touches[1])) < minDistanceDelta)
                return;
            distanceDelta *= scaleSensitivity/Screen.width;
            scaleChangeVector.Set(distanceDelta, distanceDelta, distanceDelta);
            transform.localScale -= scaleChangeVector;
            if (transform.localScale.x < minScale) 
                transform.localScale = new Vector3(minScale, minScale, minScale);
            else if(transform.localScale.x > maxScale)
                transform.localScale = new Vector3(maxScale, maxScale, maxScale);
        }
    }

    private float CalculateDistanceDelta(Touch touch1, Touch touch2)
    {
        float oldDistance, distance;
        Vector2 touch1OldPosition, touch2OldPosition;
        touch1OldPosition = touch1.position - touch1.deltaPosition;
        touch2OldPosition = touch2.position - touch2.deltaPosition;
        oldDistance = Vector2.Distance(touch1OldPosition, touch2OldPosition);
        distance = Vector2.Distance(touch1.position, touch2.position);
        return oldDistance - distance;
    }
}
