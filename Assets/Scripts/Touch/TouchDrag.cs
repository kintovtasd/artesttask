using System.Collections;
using System.Collections.Generic;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
using UnityEngine;

[RequireComponent(typeof(TouchSelectable))]
public class TouchDrag : MonoBehaviour
{
    private TouchSelectable selectable;
    private List<ARRaycastHit> hits = new List<ARRaycastHit>();
    private ARRaycastManager ARRaycast;
    private ARPlane plane;
    private ARPlaneManager planeManager;
    void Awake()
    {
        selectable = GetComponent<TouchSelectable>();
        ARRaycast = FindObjectOfType<ARRaycastManager>();
        planeManager = FindObjectOfType<ARPlaneManager>();
    }


    void Update()
    {
        if (selectable.isSelected && Input.touchCount == 1 && Input.touches[0].phase == TouchPhase.Moved)
        {
            if (ARRaycast.Raycast(Input.touches[0].position, hits, TrackableType.PlaneWithinPolygon))
            {
                moveObject(hits[0]);
            }
        }
    }

    private void moveObject(ARRaycastHit raycastHit)
    {
        plane = planeManager.GetPlane(raycastHit.trackableId);
        transform.position = raycastHit.pose.position;
        transform.up = plane.normal;
    }
}
