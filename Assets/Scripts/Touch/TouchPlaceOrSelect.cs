using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

[RequireComponent(typeof(ARRaycastManager))]
[RequireComponent(typeof(ARPlaneManager))]
public class TouchPlaceOrSelect : MonoBehaviour
{
    public Text infoText;
    public GameObject prefabToPlace;

    private Ray ray;
    private ARRaycastManager ARRaycast;
    private ARPlaneManager planeManager;
    private ARPlane plane;
    private List<ARRaycastHit> hits = new List<ARRaycastHit>();
    private Component touchSelectable;

    void Awake()
    {
        ARRaycast = GetComponent<ARRaycastManager>();
        planeManager = GetComponent<ARPlaneManager>();
    }

    void Update()
    {
         foreach (Touch touch in Input.touches)
         {
            if (touch.position.IsPointOverUIObject()) return;
            
            if (touch.phase == TouchPhase.Began)
            {
                ray = Camera.main.ScreenPointToRay(Input.touches[0].position);
                if (Physics.Raycast(ray, out RaycastHit hit))
                {
                    if (hit.collider.tag == "PlacedObject")
                    {
                        if (hit.collider.gameObject.TryGetComponent(typeof(TouchSelectable), out touchSelectable)) ((TouchSelectable)touchSelectable).isSelected = true;
                        return;
                    }
                }
            }
         }

        if (Input.touchCount == 1 && Input.touches[0].phase == TouchPhase.Began)
        {
            if (ARRaycast.Raycast(Input.touches[0].position, hits, TrackableType.PlaneWithinPolygon))
            {
                placeObject(hits[0]);
            }
        }
    }

    private void placeObject(ARRaycastHit raycastHit)
    {
        plane = planeManager.GetPlane(raycastHit.trackableId);
        //infoText.text = DetectPlaneType(plane);
        GameObject prefab = Instantiate(prefabToPlace, raycastHit.pose.position, Quaternion.identity);
        prefab.transform.up = plane.normal;
    }

    private string DetectPlaneType(ARPlane plane)
    {
        return plane.alignment.ToString();
    }

    public void ChangePrefabToPlace(GameObject prefab)
    {
        prefabToPlace = prefab;
    }
}
