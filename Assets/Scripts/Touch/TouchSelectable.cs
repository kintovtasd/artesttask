using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchSelectable : MonoBehaviour
{
    public bool isSelected = false;

    private void Update()
    {
        if (Input.touchCount == 0) isSelected = false;
    }
    
}
