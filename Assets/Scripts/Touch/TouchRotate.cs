using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

[RequireComponent(typeof(TouchSelectable))]
public class TouchRotate : MonoBehaviour
{
    public float maxDistanceDelta;
    public float rotationSensitivity;
    private TouchSelectable selectable;

    void Awake()
    {
        selectable = GetComponent<TouchSelectable>();
    }

    void Update()
    {
        if (selectable.isSelected && Input.touchCount == 2)
        {
            if ((Input.touches[0].phase != TouchPhase.Moved || Input.touches[1].phase != TouchPhase.Moved) ||
                Mathf.Abs(CalculateDistanceDelta(Input.touches[0], Input.touches[1])) > maxDistanceDelta)
                return;
            transform.Rotate(0, Input.touches[0].deltaPosition.x / Screen.width * rotationSensitivity, 0, Space.Self);
        }
    }

    private float CalculateDistanceDelta(Touch touch1, Touch touch2)
    {
        float oldDistance, distance;
        Vector2 touch1OldPosition, touch2OldPosition;
        touch1OldPosition = touch1.position - touch1.deltaPosition;
        touch2OldPosition = touch2.position - touch2.deltaPosition;
        oldDistance = Vector2.Distance(touch1OldPosition, touch2OldPosition);
        distance = Vector2.Distance(touch1.position, touch2.position);
        return oldDistance - distance;
    }
}
